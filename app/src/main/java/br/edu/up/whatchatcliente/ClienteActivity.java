package br.edu.up.whatchatcliente;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ClienteActivity extends AppCompatActivity {

  String porta = "8080";
  EditText txtServidor;
  EditText txtChat;
  EditText txtMensagem;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    txtServidor = (EditText) findViewById(R.id.txtServidor);
    txtChat = (EditText) findViewById(R.id.txtChat);
    txtMensagem = (EditText) findViewById(R.id.txtMensagem);

  }

  public void onClickEnviar(View v){

    String mensagem = txtMensagem.getText().toString();
    String servidor = txtServidor.getText().toString();

    txtChat.append("\nCliente: " + mensagem);
    txtMensagem.setText("");

    ClienteTask myClientTask = new ClienteTask();
    myClientTask.execute(mensagem, servidor, porta);
  }


  public class ClienteTask extends AsyncTask<String, Void, String> {

    @Override
    protected String doInBackground(String... params) {

      String mensagemEnviar = params[0];
      String servidor = params[1];
      int porta = Integer.parseInt(params[2]);

      Socket socket = null;
      String mensagemRecebida = null;
      DataOutputStream dos = null;
      DataInputStream dis = null;

      try {
        socket = new Socket(servidor, porta);

        dos = new DataOutputStream(socket.getOutputStream());
        Log.d("Cliente", "mensagemEnviar: " + mensagemEnviar);
        if (null != mensagemEnviar && !"".equals(mensagemEnviar)) {
          dos.writeUTF(mensagemEnviar);
        }

        dis = new DataInputStream(socket.getInputStream());
        try {
          mensagemRecebida = dis.readUTF();
        } catch (IOException e){
          //Ignorada;
          mensagemRecebida = null;
        }
      } catch (Exception e) {
        e.printStackTrace();
      } finally{

        if (dos != null){
          try {
            dos.close();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }

        if (dis != null){
          try {
            dis.close();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }

        if(socket != null){
          try {
            socket.close();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
      }
      return mensagemRecebida;
    }

    @Override
    protected void onPostExecute(String mensagemRecebida) {
      Log.d("Cliente", "mensagemRecebida: " + mensagemRecebida);
      if (null != mensagemRecebida && !"".equals(mensagemRecebida)) {
        txtChat.append("\nServidor: " + mensagemRecebida);
      }
    }
  }
}